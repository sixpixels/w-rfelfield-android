package wurfelfield;

import com.jme3.app.SimpleApplication;
import com.jme3.asset.plugins.ZipLocator;
import com.jme3.bullet.BulletAppState;
import com.jme3.bullet.collision.shapes.CapsuleCollisionShape;
import com.jme3.bullet.collision.shapes.CollisionShape;
import com.jme3.bullet.control.CharacterControl;
import com.jme3.bullet.control.RigidBodyControl;
import com.jme3.bullet.util.CollisionShapeFactory;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.light.AmbientLight;
import com.jme3.light.DirectionalLight;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Quaternion;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.network.Client;
import com.jme3.network.Network;
import com.jme3.network.serializing.Serializer;
import com.jme3.renderer.Camera;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Box;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;

/**
 * test
 * @author normenhansen
 */
public class Main extends SimpleApplication {
        
        ClientListener cl;
        static String ip;
        Node scene;
        Node dmgNode;
        Client myClient;
        InputControl i;
        BulletAppState bulletAppState;
        Player main;
        Player opponent;
        boolean hitmarker;
    public static void main(String[] args) {
        JOptionPane j = new JOptionPane();
        j.setInputValue(ip);
        j.showInputDialog("Bitte Server IP eingeben");
        Main app = new Main();
        app.start();
    }
    
    public void addLight(){
    // We add light so we see the scene
       
    AmbientLight al = new AmbientLight();
    al.setColor(ColorRGBA.White.mult(1.3f));
    rootNode.addLight(al);
 
    DirectionalLight dl = new DirectionalLight();
    dl.setColor(ColorRGBA.White);
    dl.setDirection(new Vector3f(2.8f, -2.8f, -2.8f).normalizeLocal());
    rootNode.addLight(dl);
        //Todo
    }
    public void addPlayers()
    {
        main = new Player(rootNode, assetManager, new Vector3f(300, 10, 0));
        opponent = new Player(rootNode, assetManager, new Vector3f(0, 10, 0));
        bulletAppState.getPhysicsSpace().add(main.playerc);
        bulletAppState.getPhysicsSpace().add(opponent.playerc);
        //Todo
    }
    public void addScene(){
        assetManager.registerLocator("town.zip", ZipLocator.class);
        Spatial underground = assetManager.loadModel("main.scene");
        underground.setLocalScale(2f);
        scene.attachChild(underground);
        CollisionShape sceneShape =
            CollisionShapeFactory.createMeshShape(scene);
        RigidBodyControl landscape;
        landscape = new RigidBodyControl(sceneShape, 0);
        scene.addControl(landscape);
        bulletAppState.getPhysicsSpace().add(scene);
        //Todo
    }
    public void addControls(){
        i = new InputControl(this.flyCam);
        inputManager.addMapping("Left", new KeyTrigger(KeyInput.KEY_A));
        inputManager.addMapping("Right", new KeyTrigger(KeyInput.KEY_D));
        inputManager.addMapping("Up", new KeyTrigger(KeyInput.KEY_W));
        inputManager.addMapping("Down", new KeyTrigger(KeyInput.KEY_S));
        inputManager.addMapping("Jump", new KeyTrigger(KeyInput.KEY_SPACE));
        inputManager.addMapping("Shoot", new MouseButtonTrigger(MouseInput.BUTTON_LEFT));

        inputManager.addListener(i, "Left");
        inputManager.addListener(i, "Right");
        inputManager.addListener(i, "Up");
        inputManager.addListener(i, "Down");
        inputManager.addListener(i, "Jump");
        inputManager.addListener(i, "Shoot");
    }
    public void updateCrossHairs() {
    guiNode.detachAllChildren();
    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
    BitmapText ch = new BitmapText(guiFont, false);
    ch.setSize(guiFont.getCharSet().getRenderedSize() * 4);
        ch.setText("+");
    if(hitmarker)
    {
        ch.setSize(guiFont.getCharSet().getRenderedSize() * 4);
        ch.setText("X");
    }
         // crosshairs
    ch.setLocalTranslation( // center
      settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 2,
      settings.getHeight() / 2 + ch.getLineHeight() / 2, 0);
    guiNode.attachChild(ch);
  }
    public void initFlash() {
    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
    BitmapText ch = new BitmapText(guiFont, false);
    ch.setColor(ColorRGBA.Orange);
    ch.setSize(guiFont.getCharSet().getRenderedSize() * 150);
    ch.setText("+"); // crosshairs
    ch.setLocalTranslation( // center
      settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 180,
      settings.getHeight() / 2 + ch.getLineHeight() / 2+300, 0);
    ch.setName("flash");
    guiNode.attachChild(ch);
  }
    public void initDamage() {
            System.out.println("KKKKKKKKKKKKKKKKKKKK");
    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
    BitmapText ch = new BitmapText(guiFont, false);
    ch.setColor(ColorRGBA.Red);
    ch.setSize(guiFont.getCharSet().getRenderedSize() * 150);
    ch.setText("="); // crosshairs
    ch.setLocalTranslation( // center
      settings.getWidth() / 2 - guiFont.getCharSet().getRenderedSize() / 3 * 180,
      settings.getHeight() / 2 + ch.getLineHeight() / 2+300, 0);
    ch.setName("damage");
    guiNode.attachChild(ch);
  }
    
    @Override
    public void simpleInitApp() {
        dmgNode = new Node();
        dmgNode.setName("dmgn");
        bulletAppState=new BulletAppState();
        stateManager.attach(bulletAppState);
        scene = new Node();
        addPlayers();
        addLight();
        addScene();
        addControls();
        updateCrossHairs();
        rootNode.attachChild(dmgNode);
        rootNode.attachChild(scene);
        this.flyCam.setMoveSpeed(0);
            
        try {
            Serializer.registerClass(StatusMessage.class);
            
            myClient = Network.connectToServer(ip, 6143);
            myClient.start();
            cl = new ClientListener();
            myClient.addMessageListener(cl, StatusMessage.class);
            //this.getCamera().setLocation(Vector3f.ZERO);
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    public void walk()
    {
        Vector3f camDir = cam.getDirection().clone().multLocal(0.1f);
        camDir=new Vector3f(camDir.x,0,camDir.z);
        Vector3f camLeft = cam.getLeft().clone().multLocal(0.1f);
        camLeft=new Vector3f(camLeft.x,0,camLeft.z);
        Vector3f walkDirection = new Vector3f();
        walkDirection.set(0, 0, 0);
        if (i.left)  { walkDirection.addLocal(camLeft); }
        if (i.right) { walkDirection.addLocal(camLeft.negate()); }
        if (i.up)    { walkDirection.addLocal(camDir); }
        if (i.down)  { walkDirection.addLocal(camDir.negate()); }
        if(i.jump) {
            main.playerc.jump();
            i.jump=false;
        }
        main.player.getChild("body").rotateUpTo(new Vector3f(0, 0, camDir.z));
        walkDirection=walkDirection.multLocal(3);
        main.playerc.setWalkDirection(walkDirection);
        main.player.getChild("body").setLocalTranslation(main.playerc.getPhysicsLocation());
        main.player.getChild("body").setLocalRotation(this.getCamera().getRotation());
        this.getCamera().setLocation(main.playerc.getPhysicsLocation());
    }
    int blzahler=0;
    int dzahler=0;
    public void shoot()
    {
        
            System.out.println(main.llife+ " "+main.life + " " + (main.llife != main.life));
        if(main.llife != main.life)
        {
            initDamage();
            dzahler = 0;
            main.llife = main.life;
        }
        if(guiNode.getChild("damage")!=null)
        {
            if(dzahler++ > 30)
            {
                guiNode.detachChildNamed("damage");
            }
        }
        if(blzahler++ > 5)
            hitmarker = false;
        if(guiNode.getChild("flash")!=null)
        {
            if(blzahler>5)
            {
                guiNode.detachChildNamed("flash");
            }
        }
        if(i.shoot){
            this.getCamera().lookAtDirection(this.getCamera().getDirection().addLocal(new Vector3f(0, 0.02f, 0)), new Vector3f(0, 1, 0));
        
            blzahler=0;
            initFlash();
            i.shoot=false;
            
            int power=100;
            // 1. Reset results list.
            CollisionResults results = new CollisionResults();
            Ray ray = new Ray(cam.getLocation(), cam.getDirection());
            rootNode.collideWith(ray, results);
            for (int j = 0; j < results.size(); j++) {
                
              float dist = results.getCollision(j).getDistance();
              String hit = results.getCollision(j).getGeometry().getName();
              power-=dist;
              if(opponent.player.hasChild(results.getCollision(j).getGeometry())){
                  hitmarker = true;
                  if(power>=0) {
                      opponent.life -= power;
                  }
                  if(opponent.life < 0)
                      opponent.player.getChild(hit).removeFromParent();
                  power -= 30;
              }
              if(scene.hasChild(results.getCollision(j).getGeometry())) //don't kill scene
                power -= 80;

            }
        }
    }
    public void communicate()
    {
        myClient.send(new StatusMessage(main, opponent, this.getCamera()));
        main.life = cl.mainlife;
        System.out.println(cl.opponentrx);
        opponent.playerc.setPhysicsLocation(new Vector3f(cl.opponentx, cl.opponenty, cl.opponentz));
        try
        {
            opponent.player.getChild("body").setLocalTranslation(opponent.playerc.getPhysicsLocation());
            opponent.player.getChild("body").lookAt(new Vector3f(cl.opponentrx, cl.opponentry, cl.opponentrz).addLocal(opponent.playerc.getPhysicsLocation()), new Vector3f(0, 1, 0));
        }
        catch(NullPointerException e)
        {
            JFrame j = new JFrame();
            JOptionPane.showMessageDialog(j, "You Won!!");
            System.exit(0);
        }
        if(main.life < 0)
        {            
            JFrame j = new JFrame();
            JOptionPane.showMessageDialog(j, "You Lose!!");
            System.exit(0);
        }
    }
    @Override
    public void simpleUpdate(float tpf) {
        updateCrossHairs();
        walk();
        shoot();
        communicate();
       
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }  
    @Override
    public void destroy() {
        try {
            myClient.close();
            super.destroy();
        } catch (Exception e) {
        }
  }
}
