/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package wurfelfield;

import com.jme3.network.AbstractMessage;
import com.jme3.network.serializing.Serializable;
import com.jme3.renderer.Camera;

/**
 *
 * @author simon
 */
@Serializable
public class StatusMessage extends AbstractMessage {
    //Player main;
    //Player opponent;
    float x,y,z, rx, ry, rz;
    int life;
    public StatusMessage(Player m, Player o, Camera c) 
    {
        x = m.playerc.getPhysicsLocation().x;
        y = m.playerc.getPhysicsLocation().y;
        z = m.playerc.getPhysicsLocation().z;
        rx = c.getDirection().x;
        ry = c.getDirection().y;
        rz = c.getDirection().z;
        life = o.life;
    } // custom constructor
    public StatusMessage() 
    {
    } // custom constructor
}
